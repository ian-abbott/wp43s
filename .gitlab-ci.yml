# SPDX-License-Identifier: GPL-3.0-only
# SPDX-FileCopyrightText: Copyright The WP43 Authors

default:
  image: ubuntu:latest

stages:
  - build
  - test
  - upload
  - release

variables:
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/wp43/${CI_COMMIT_TAG}"
  GIT_SUBMODULE_STRATEGY: recursive
  DEBIAN_FRONTEND: noninteractive

macOS:
  stage: build
  image: macos-12-xcode-14
  script:
    - HOMEBREW_NO_AUTO_UPDATE=1 HOMEBREW_NO_INSTALL_CLEANUP=1 HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK=1 brew install ccache ninja python@3.9 gtk+3 gmp pulseaudio
    - python3 -m pip install meson
    - make dist_macos
  artifacts:
    expire_in: 1 day
    paths:
      - wp43-macos.zip
  tags:
    - saas-macos-medium-m1

linux:
  stage: build
  script:
    - apt-get update -qq
    - apt-get install -qq -y git python3-pip ninja-build pkg-config build-essential libgtk-3-dev libgmp-dev libpulse-dev
    - python3 -m pip install meson
    - make sim
  tags:
    - saas-linux-small-amd64

windows:
  stage: build
  script:
    - C:\msys64\msys2_shell.cmd -defterm -mingw64 -no-start -here -c "make dist_windows"
  artifacts:
    expire_in: 1 day
    paths:
      - wp43-windows.zip
  tags:
    - wp43_windows

dmcp:
  stage: build
  script:
    - apt-get update -qq
    - apt-get install -qq -y git python3-pip ninja-build pkg-config build-essential zip libgtk-3-dev libgmp-dev gcc-arm-none-eabi
    - python3 -m pip install meson
    - make dist_dmcp
  artifacts:
    expire_in: 1 day
    paths:
      - wp43-dmcp.zip
  tags:
    - saas-linux-small-amd64

testSuite:
  stage: test
  coverage: '/^TOTAL.*\s+(\d+\%)$/'
  script:
    - apt-get update -qq
    - apt-get install -qq -y git python3-pip ninja-build pkg-config build-essential libgtk-3-dev libgmp-dev gcovr
    - python3 -m pip install meson
    - make test
    - make coverage
  artifacts:
    expire_in: 5 day
    paths:
      - testSuiteJunit.xml
    reports:
      junit: testSuiteJunit.xml
      coverage_report:
        coverage_format: cobertura
        path: build.sim/meson-logs/coverage.xml
  tags:
    - saas-linux-small-amd64

codeDocs:
  stage: test
  script:
    - apt-get update -qq
    - apt-get install -qq -y git python3-pip ninja-build pkg-config build-essential libgtk-3-dev libgmp-dev doxygen
    - python3 -m pip install meson Sphinx breathe furo
    - make docs

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file wp43-dmcp.zip ${PACKAGE_REGISTRY_URL}/wp43-dmcp-${CI_COMMIT_TAG}.zip
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file wp43-macos.zip ${PACKAGE_REGISTRY_URL}/wp43-macos-${CI_COMMIT_TAG}.zip
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file wp43-windows.zip ${PACKAGE_REGISTRY_URL}/wp43-windows-${CI_COMMIT_TAG}.zip
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file docs/ReferenceManual.pdf ${PACKAGE_REGISTRY_URL}/wp43-reference-manual-${CI_COMMIT_TAG}.pdf
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file docs/OwnersManual.pdf ${PACKAGE_REGISTRY_URL}/wp43-owners-manual-${CI_COMMIT_TAG}.pdf
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file docs/Addendum.md ${PACKAGE_REGISTRY_URL}/wp43-manual-addenda-and-corrigenda-${CI_COMMIT_TAG}.md
  tags:
    - saas-linux-small-amd64

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"Windows Simulator\",\"url\":\"${PACKAGE_REGISTRY_URL}/wp43-windows-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"Firmware for DMCP\",\"url\":\"${PACKAGE_REGISTRY_URL}/wp43-dmcp-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"Reference Manual\",\"url\":\"${PACKAGE_REGISTRY_URL}/wp43-reference-manual-${CI_COMMIT_TAG}.pdf\"}" \
        --assets-link "{\"name\":\"Owner's Manual\",\"url\":\"${PACKAGE_REGISTRY_URL}/wp43-owners-manual-${CI_COMMIT_TAG}.pdf\"}" \
        --assets-link "{\"name\":\"Addenda and Corrigenda\",\"url\":\"${PACKAGE_REGISTRY_URL}/wp43-manual-addenda-and-corrigenda-${CI_COMMIT_TAG}.md\"}"
  tags:
    - saas-linux-small-amd64
