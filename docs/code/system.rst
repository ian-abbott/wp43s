System
======

Hardware abstraction layer for system functions. These are often more complex functions
that compose a complete piece of functionality. At the moment this is only for
``systemScreenshot`` which takes a screenshot, and ``systemMaker`` to identify the maker
of the platform.

Functions
---------

.. doxygenfile:: hal/system.h
