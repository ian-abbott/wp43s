// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file distributions/distributions.h
 */
#if !defined(DISTRIBUTIONS_H)
  #define DISTRIBUTIONS_H

  #include "binomial.h"
  #include "cauchy.h"
  #include "chi2.h"
  #include "exponential.h"
  #include "f.h"
  #include "geometric.h"
  #include "hyper.h"
  #include "logistic.h"
  #include "negBinom.h"
  #include "normal.h"
  #include "poisson.h"
  #include "t.h"
  #include "weibull.h"

#endif // !DISTRIBUTIONS_H
