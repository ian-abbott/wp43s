// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file programming/clcvar.h
 */
#if !defined(CLCVAR_H)
  #define CLCVAR_H

  #include <stdint.h>

  void fnClCVar(uint16_t unusedButMandatoryParameter);

#endif // !CLCVAR_H
