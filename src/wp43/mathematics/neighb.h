// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/neighb.h
 */
#if !defined(NEIGHB_H)
  #define NEIGHB_H

  #include <stdint.h>

  void fnNeighb(uint16_t unusedButMandatoryParameter);

#endif // !NEIGHB_H
