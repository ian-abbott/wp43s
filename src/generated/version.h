// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/*********************************************************************
 * This file is not used by the build system but is present for IDEs *
 *********************************************************************/

/**
 * \file version.h
 */
#if !defined(VERSION_H)
  #define VERSION_H

  #define VERSION_STRING "custom" STD_SPACE_3_PER_EM "build" STD_SPACE_3_PER_EM "(unknown source)"
  #define VERSION_SHORT  "(unknown source)"

#endif // !VERSION_H
